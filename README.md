# Information Retrieval Evaluation at AFIRM 2019

This repository contains slides, source code, and additional material complementing the lectures:

* _Information Retrieval Evaluation_ by Nicola Ferro
* _From Offline to Online Evaluation_ by Maria Maistro
* _IR Evaluation Lab_ by Nicola Ferro and Maria Maistro

held at the [ACM SIGIR/SIGKDD Africa Summer School on Machine Learning for Data Mining and Search](http://sigir.org/afirm2019/) (AFIRM 2019), 14-18 January 2019, Cape Town, South Africa.

Copyright and license information can be found in the file LICENSE. 
Additional information can be found in the file NOTICE.


The repository also contains a `trec_eval.9.0` folder where the `trec_eval` source code is stored. 
The code is copied from [https://trec.nist.gov/trec_eval/](https://trec.nist.gov/trec_eval/). 
Due to the US Government shutdown during AFIRM 2019, the TREC Web site was not accessible and it was thus necessary to clone the `trec_eval` source code here.
Note that the file `trec_eval.c` has been modified (thanks to Hussein Suleman) to fix a segmentation fault problem due int/long issues on 64-bit architectures. The original `trec_eval.c` files has been renamed `trec_eval.c.orig`.

