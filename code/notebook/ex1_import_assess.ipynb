{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<pre> \n",
    " Copyright 2018-2019 University of Padua, Italy\n",
    "\n",
    " Licensed under the Apache License, Version 2.0 (the \"License\");\n",
    " you may not use this file except in compliance with the License.\n",
    " You may obtain a copy of the License at\n",
    "\n",
    "     http://www.apache.org/licenses/LICENSE-2.0\n",
    "\n",
    " Unless required by applicable law or agreed to in writing, software\n",
    " distributed under the License is distributed on an \"AS IS\" BASIS,\n",
    " WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n",
    " See the License for the specific language governing permissions and\n",
    " limitations under the License.\n",
    "\n",
    " Author: Nicola Ferro (ferro@dei.unipd.it)\n",
    " Author: Maria Maistro (maistro@dei.unipd.it)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1>Configuration of the script</h1>\n",
    "\n",
    "<p>Configure the script as follows:</p>\n",
    "<ul>\n",
    "    <li><code>qrels_file</code>: path to the text file containing the qrels/pool;</li>\n",
    "    <li><code>run_file</code>: path to the text file containing the run to evaluate.</li>\n",
    "</ul>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "from operator import itemgetter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# path to the qrels\n",
    "qrels_file = \"../../data/collection/CLEF2009-Monolingual-Persian/pool/AH-PERSIAN-CLEF2009.txt\"\n",
    "\n",
    "# path to the run\n",
    "run_file = \"../../data/collection/CLEF2009-Monolingual-Persian/runs/JHUFA4R100TD.txt\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1>Importing a run</h1>\n",
    "\n",
    "<p>The script assumes the file to be in the standard <code>trec_eval</code> format:\n",
    "\n",
    "<pre>&lt;topic_id&gt; Q0 &lt;doc_id&gt; &lt;rank&gt; &lt;score&gt; &lt;run_id&gt;</pre>\n",
    "\t\n",
    "where:</p>\n",
    "<ul>\n",
    "    <li>fields are assumed to be separated by either a space or a tab;</li>\n",
    "    <li><code>topic_id</code> - a string, specifying the identifier of a topic;</li>\n",
    "    <li><code>Q0</code> - a constant (unused) field. It is discarded during the import;</li>\n",
    "    <li><code>doc_id</code> - a string, specifying the identifier of a document;</li>\n",
    "    <li><code>rank</code> - an integer (unused), specifying the rank of a document for a topic. It is discarded during the import;</li>\n",
    "    <li><code>score</code> - a numeric value, specifying the score of a document for a topic;</li>\n",
    "    <li><code>run_id</code> - a string (unused), specifying the identifier of the run. It is discarded during the import.</li>\n",
    "</ul>\n",
    "\n",
    "<p>The script parses each line of the input file and maps it to a tuple\n",
    "    \n",
    "<pre>tup = (topic_id, doc_id, score)</pre>\n",
    "    \n",
    "and represents the whole run as a list of such tuples.</p>\n",
    "   \n",
    "<p>The script adopts the same ordering as <code>trec_eval</code>,\n",
    "    i.e. it sorts tuples by descending order of <code>score</code> and descending \n",
    "    lexicographical order of <code>doc_id</code>. Moreover, as done by <code>trec_eval</code>, it represents the document <code>score</code> as a <code>float</code>.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "First ten lines of the run\n",
      "[('601', 'H-790610-49274S5', 91.8931430844419), ('601', 'H-790805-49795S3', 91.8929194377263), ('601', 'H-811014-59441S3', 91.7274806713167), ('601', 'H-761130-39873S1', 91.6842150453922), ('601', 'H-770924-43332S3', 91.5829782329668), ('601', 'H-810415-56213S3', 91.5562308886449), ('601', 'H-801221-54467S10', 91.5490098403379), ('601', 'H-810426-56410S1', 91.5446888669918), ('601', 'H-800625-52513S1', 91.5264703204511), ('601', 'H-810405-56034S1', 91.5190342211964)]\n"
     ]
    }
   ],
   "source": [
    "# read the input file as a whole\n",
    "with open(run_file) as f:\n",
    "    input_file = f.readlines()\n",
    "        \n",
    "# the run as a list of tuples, where each element is a parsed line from the input file\n",
    "run = []\n",
    "    \n",
    "# parse each line, turn it into a tuple, and add it to the list\t\n",
    "for line in input_file:\n",
    "    # parse the line    \n",
    "    topic_id, _, doc_id, _, score, _ = line.strip().split()\n",
    "\n",
    "    # create a tuple\n",
    "    tup = (topic_id, doc_id, float(score))\n",
    "\n",
    "    # add the tuple to the list\n",
    "    run.append(tup)\n",
    "    \n",
    "# sort the tuples by topic_id ascending, score descending, doc_id descending\n",
    "# this is the same ordering used by trec_eval\n",
    "# remember that sort in Python is stable\n",
    "run.sort(key=itemgetter(1), reverse=True)         # doc_id descending\n",
    "run.sort(key=itemgetter(2), reverse=True)         # score descending\n",
    "run.sort(key=itemgetter(0))                       # topic_id ascending\n",
    "\n",
    "# print the first ten lines of the run\n",
    "print(\"First ten lines of the run\")\n",
    "print(run[:10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1>Importing qrels</h1>\n",
    "\n",
    "<p>The script assumes the file to be in the standard <code>trec_eval</code> format:\n",
    "\n",
    "<pre>&lt;topic_id&gt; 0 &lt;doc_id&gt; &lt;rel&gt;</pre>\n",
    "\t\n",
    "where:</p>\n",
    "<ul>\n",
    "    <li>fields are assumed to be separated by either a space or a tab;</li>\n",
    "    <li><code>topic_id</code> - a string, specifying the identifier of a topic;</li>\n",
    "    <li><code>0</code> - a constant (unused) field. It is discarded during the import;</li>\n",
    "    <li><code>doc_id</code> - a string, specifying the identifier of a document;</li>\n",
    "    <li><code>rel</code> - the relevance degree of a document for a topic; typically 0\n",
    "        for not relevant documents and 1 for relevant ones. <code>rel</code> is assumed\n",
    "    to be an integer number.</li>\n",
    "</ul>\n",
    "\n",
    "<p>The script parses each line of the input file and stores it into a dictionary where keys \n",
    "    are <code>(topic_id, doc_id)</code> tuples and values are <code>rel</code>evance degrees.\n",
    "    This dictionary is useful later on when we will need to user the qrels to judge a run.</p>\n",
    "    \n",
    "<p>The script also computes the <i>recall base</i>, i.e. the total number of relevant documents, \n",
    "    for each topic and stores it into a dictionary where keys are <code>topic_id</code> and values are\n",
    "    the total number of relevant documents.</p>\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Ten entries of the qrels\n",
      "[(('601', 'H-750501-1947S1'), 0), (('601', 'H-750503-2128S1'), 0), (('601', 'H-750506-2290S1'), 0), (('601', 'H-750506-2299S1'), 0), (('601', 'H-750509-2577S1'), 0), (('601', 'H-750514-2769S1'), 1), (('601', 'H-750514-2790S1'), 1), (('601', 'H-750515-2834S1'), 0), (('601', 'H-750517-3015S1'), 1), (('601', 'H-750518-3074S1'), 0)]\n",
      "\n",
      "Ten entries of the recall base\n",
      "[('601', 89), ('602', 93), ('603', 63), ('604', 73), ('605', 134), ('606', 135), ('607', 38), ('608', 177), ('609', 27), ('610', 8)]\n"
     ]
    }
   ],
   "source": [
    "# read the input file as a whole\n",
    "with open(qrels_file) as f:\n",
    "    input_file = f.readlines()\n",
    "    \n",
    "# the qrels as a dictionary where keys are (topic_id, doc_id) pairs and values are the relevance degrees\n",
    "qrels = {}\n",
    "\n",
    "# the recall base for each topic, i.e. the total number of relevant documents,\n",
    "# as a dictionary where keys are topic_id and values are the total number of relevant documents\n",
    "rb = {}\n",
    "    \n",
    "# parse each line and populate the corresponding dictionaries\n",
    "for line in input_file:\n",
    "    # parse the line    \n",
    "    topic_id, _, doc_id, rel = line.strip().split()\n",
    "\n",
    "    # add the relevance degree for the tuple (topic_id, doc_id)\n",
    "    qrels[(topic_id, doc_id)] = int(rel)\n",
    "    \n",
    "    # get current value for the topic (set it to 0 if it does not exist yet)\n",
    "    # add the current relevance degree to it\n",
    "    # store it back into the dictionary\n",
    "    rb[topic_id] = rb.get(topic_id, 0) + int(rel)\n",
    "    \n",
    "# print ten entries of the qrels\n",
    "print(\"Ten entries of the qrels\")\n",
    "print( [v for v in qrels.items()][:10] )\n",
    "\n",
    "# print ten entries of the recall base\n",
    "print(\"\\nTen entries of the recall base\")\n",
    "print([v for v in rb.items()][:10]) \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1>Assessing the run</h1>\n",
    "\n",
    "<p>A <i>judged run</i> is represented as a dictionary where keys are <code>topic_id</code> and values are lists of judged documents, i.e. lists of relevance degrees corresponding to each document.</p>\n",
    "\n",
    "<p>To judge a run, for each <code>(topic_id, doc_id, score)</code> tuple we look up that tuple in the <code>qrels</code> dictionary and we append the corresponding <code>rel</code> values to the list of judged documents.</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Ten entries of the judged run\n",
      "[('601', [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), ('602', [1, 1, 1, 1, 1, 1, 1, 1, 0, 0]), ('603', [0, 0, 1, 0, 1, 0, 1, 0, 0, 0]), ('604', [0, 0, 0, 0, 1, 1, 1, 0, 1, 0]), ('605', [1, 1, 1, 1, 1, 0, 1, 1, 1, 1]), ('606', [0, 1, 1, 0, 1, 1, 1, 0, 1, 1]), ('607', [1, 1, 1, 1, 1, 0, 1, 1, 1, 1]), ('608', [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]), ('609', [1, 0, 1, 1, 1, 0, 0, 1, 0, 0]), ('610', [1, 0, 1, 1, 0, 1, 0, 0, 1, 0])]\n"
     ]
    }
   ],
   "source": [
    "# the judged run as a dictionary where keys are topic_id and values are lists of judged documents, \n",
    "# i.e. lists of relevance degrees corresponding to each document\n",
    "jrun = {}\n",
    "\n",
    "for tup in run:\n",
    "    topic_id = tup[0]\n",
    "    doc_id = tup[1]\n",
    "    \n",
    "    # get the list of judged documents for the topic\n",
    "    # if it does not exist yet, create an empty list\n",
    "    l = jrun.get(topic_id, [])\n",
    "    \n",
    "    # append to the list the judgement for the current (topic_id, doc_id) pair\n",
    "    # if the pair is not in the qrels, i.e. it is unjudged, assume it is not relevant\n",
    "    l.append(qrels.get((topic_id, doc_id), 0))\n",
    "    \n",
    "    # copy back the list in the judged run\n",
    "    jrun[topic_id] = l\n",
    "\n",
    "# print ten entries of the judeged run, first ten judged documents\n",
    "print(\"\\nTen entries of the judged run\")\n",
    "print([(k, v[:10]) for (k, v) in jrun.items()][:10]) \n",
    "        "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
