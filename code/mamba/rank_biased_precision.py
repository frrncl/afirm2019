# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)

from math import pow

def rank_biased_precision(jrun, p):
	"""Computes rank-biased precision.

	Args:
		jrun: an assessed run.
		p: the persistence of the user.

	Returns:
		The value of the measure.
	"""

	# the measure as a list of (topic_id, score) tuples
	measure = []

	# for each topic
	for topic_id in jrun.keys():
	    
	    # the rank-biased precision score
	    score = 0
	    
	    # for each rank position
	    for i in range(len(jrun[topic_id])):
	               
	        # add the contribution of the i-th rank position, if relevant
	        score += pow(p * jrun[topic_id][i], i)
	                
	    # normalize the score
	    score = (1 - p) * score
	                
	    # append the score of the measure for the given topic
	    measure.append((topic_id, score))


	return measure
