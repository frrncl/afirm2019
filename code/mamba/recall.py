# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


def recall(jrun, rb, k = 10):
	"""Computes recall at the given document cut-off value.

	Args:
		jrun: an assessed run.
		rb: the recall base.
		k: the document cut-off value at which the measure is computed.
		   The default is 10.

	Returns:
		The value of the measure.
	"""

	# the measure as a list of (topic_id, score) tuples
	measure = []

	for topic_id in jrun.keys():
    
	    # recall@k is given by the sum of the first k elements 
	    # of the list of judged documents for a topic divided by recall base
	    score = sum(jrun[topic_id][:k])/rb[topic_id]
	     
	    # append the score of the measure for the given topic
	    measure.append((topic_id, score)) 


	return measure
