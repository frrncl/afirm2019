# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


def import_qrels(qrels_file):
	"""Imports a pool, i.e. qrels, from a text file in the standard 
	trec_eval format.

	It assumes the file to be in the following format:

	<topic_id> 0 <doc_id> <rel>
	
	where
	- fields are assumed to be separated by either a space or a tab;
	- topic_id - a string, specifying the identifier of a topic;
	- 0 - a constant (unused) field. It is discarded during the import;
	- doc_id - a string, specifying the identifier of a document;
	- rel - the relevance degree of a document for a topic; typically 0 for 
	  not relevant documents and 1 for relevant ones. rel is assumed to be an 
	  integer number.

	Args:
		qrels_file: path to the file to be imported.

	Returns:
		The script parses each line of the input file and stores it into a 
		dictionary where keys are (topic_id, doc_id) tuples and values are 
		relevance degrees. This dictionary is useful later on when we will need 
		to user the qrels to judge a run.


		The script also computes the recall base, i.e. the total number of 
		relevant documents, for each topic and stores it into a dictionary 
		where keys are topic_id and values are the total number of relevant 
		documents.

	Raises:
		IOError: An error occurred accessing the input file.
	"""

	# read the input file as a whole
	with open(qrels_file) as f:
	    input_file = f.readlines()
	    
	# the qrels as a dictionary where keys are (topic_id, doc_id) pairs and values are the relevance degrees
	qrels = {}

	# the recall base for each topic, i.e. the total number of relevant documents,
	# as a dictionary where keys are topic_id and values are the total number of relevant documents
	rb = {}
	    
	# parse each line and populate the corresponding dictionaries
	for line in input_file:
	    # parse the line    
	    topic_id, _, doc_id, rel = line.strip().split()

	    # add the relevance degree for the tuple (topic_id, doc_id)
	    qrels[(topic_id, doc_id)] = int(rel)
	    
	    # get current value for the topic (set it to 0 if it does not exist yet)
	    # add the current relevance degree to it
	    # store it back into the dictionary
	    rb[topic_id] = rb.get(topic_id, 0) + int(rel)


	return qrels, rb
