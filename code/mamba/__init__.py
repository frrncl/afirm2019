# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


# functions to import runs, qrels and assess runs
from mamba.import_run import import_run
from mamba.import_qrels import import_qrels
from mamba.assess_run import assess_run

# functions to compute evaluation measures
from mamba.precision import precision
from mamba.recall import recall
from mamba.rprec import rprec
from mamba.average_precision import average_precision
from mamba.discounted_cumulated_gain import discounted_cumulated_gain
from mamba.rank_biased_precision import rank_biased_precision

# function to parse click log data
from mamba.clicklog_parser import clicklog_parser