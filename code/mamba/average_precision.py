# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


def average_precision(jrun, rb):
	"""Computes average precision.

	Args:
		jrun: an assessed run.
		rb: the recall base.

	Returns:
		The value of the measure.
	"""

	# the measure as a list of (topic_id, score) tuples
	measure = []

	# for each topic
	for topic_id in jrun.keys():
	    
	    # the total number of relevant documents so far
	    rel = 0
	    
	    # the sum of the precision at each relevant retrieved document
	    sumPrec = 0
	    
	    # for each rank position
	    for i in range(len(jrun[topic_id])):
	        
	        # if it is a relevant document
	        if (jrun[topic_id][i]):
	            
	            # update the total number of relevant documents 
	            # up to that rank position
	            rel += jrun[topic_id][i]
	            
	            # update the sum of precision with
	            # precision at the current rank position
	            # remember that Python uses 0-based indexing
	            # while rank positions start from 1
	            sumPrec += rel/(i+1)
	            
	    # average precision is the sum of precisions divided by the recall base
	    score = sumPrec/rb[topic_id]
	        
	    # append the score of the measure for the given topic
	    measure.append((topic_id, score))


	return measure
