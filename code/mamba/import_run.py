# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)

from operator import itemgetter

def import_run(run_file):
	"""Imports a run from a text file in the standard trec_eval format.

	It assumes the file to be in the following format:

	<topic-id> Q0 <document-id> <rank> <score> <run-id>
	
	where
	- fields are assumed to be separated by either a space or a tab. Multiple
	delimiters are treated as one;
	- topic-id - a string, specifying the identifier of a topic;
	- Q0 - a constant (unused) field. It is discarded during the import;
	- document-id - a string, specifying the identifier of a document;
	- rank - an integer, specifying the rank of a document for a topic;
	- score - a numeric value, specifying the score of a document for a topic;
	- run-id - a string, specifying the identifier of the run. 

	Args:
		run_file: path to the file to be imported.

	Returns:
		The script parses each line of the input file and maps it to a tuple

		tup = (topic_id, doc_id, score)
	
		and represents the whole run as a list of such tuples.


		The script adopts the same ordering as trec_eval, i.e. it sorts tuples 
		by descending order of score and descending lexicographical order of 
		doc_id. Moreover, as done by trec_eval, it represents the document 
		score as a float.

	Raises:
		IOError: An error occurred accessing the input file.
	"""

	# read the input file as a whole
	with open(run_file) as f:
		input_file = f.readlines()
			
	# the run as a list of tuples, where each element is a parsed line from the input file
	run = []
		
	# parse each line, turn it into a tuple, and add it to the list	
	for line in input_file:
		# parse the line    
		topic_id, _, doc_id, _, score, _ = line.strip().split()

		# create a tuple
		tup = (topic_id, doc_id, float(score))

		# add the tuple to the list
		run.append(tup)
		
	# sort the tuples by topic_id ascending, score descending, doc_id descending
	# this is the same ordering used by trec_eval
	# remember that sort in Python is stable
	run.sort(key=itemgetter(1), reverse=True)         # doc_id descending
	run.sort(key=itemgetter(2), reverse=True)         # score descending
	run.sort(key=itemgetter(0))                       # topic_id ascending


	return run
