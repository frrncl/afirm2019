# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


def clicklog_parser(log_file):
	"""Imports a set of queries from a tsv file.

	The script assumes the file to be in the following format for queries:

	session_id	time_passed	action_type	query_id	region_id	url_1	url_2	
	url_3	url_4	url_5	url_6	url_7	url_8	url_9	url_10  
	
	where:
	- fields are assumed to be separated by a tab;
	- each line is sorted by session_id and by time_passed within the session;
	- session_id - an integer, specifying the identifier of a session;
	- time_passed - an integer, specifying the time passed from the previous 
	  interaction in the same section;
	- action_type - a character, set to Q for queries;
	- query_id - an integer, specifying the identifier of a query;
	- region_id - an integer, specifying the identifier of a region;
	- url_x - an integer, specifying the identifier of the document at rank 
	  position x. Each query is associated to a list of 10 results, i.e. the 
	  documents shown on the first page of the system.
	
	The script assumes the file to be in the following format for clicks:

	session_id  time_passed  action_type  url_id

	where:
	- fields are assumed to be separated by a tab;
	- each line is sorted by session_id and by time_passed within the session;
	- session_id - an integer, specifying the identifier of a session;
	- time_passed - an integer, specifying the time passed from the previous 
	  interaction in the same section;
	- action_type - a character, set to C for clicks;
	- url_id - an integer, specifying the identifier of the clicked document.

	Args:
		log_file: path to the file to be imported.

	Returns:
		The script parses each query of the input file and maps it to a 
		dictionary where the keys are tuples defined as follows:

		key = (session_id, query_id, region_id)

		and the values are arrays with 10 entries, which represent the list of 
		results. Each entry of the array is an array with three entries defined as follows:
		
		array_entry = [url_id, click, time_spent]
		
		where:
		- url_id - a character, the identifier of the document;
		- click - an integer, specifies the number of time that the document was
		  clicked;
		- time_spent - an integer, it is the (cumulated) time spent by the user 
		  in visiting the document.

	Raises:
		IOError: An error occurred accessing the input file.
	"""

	# read the input file as a whole
	with open(log_file) as f:
		input_file = f.readlines()
		
	# the queries as a dictionary where keys are (session_id, query_id, 
	# region_id) tuples and values are arrays with ten entries
	queries = {}

	# Initialize the session id to None
	session_id = None

	# Flag variable to check wether there are dwell time to process
	click_to_process = 0


	# parse each line and populate the corresponding dictionaries
	for line in input_file:
		# parse the line, split the fields with the tab 
		parsed_line = line.strip().split("\t")
		
		# Check the action type field
		if parsed_line[2] == "Q":
			
			# Check wheter we are still within the same session
			if session_id == parsed_line[0]:
				# Check whether there are click to process
				if click_to_process:
					# Compute the time spent visiting the document
					time_spent = int(parsed_line[1]) - previous_time_passed
					# Update the corresponding array entry in the hash map
					queries[(session_id, query_id, region_id)][previous_index][2] += time_spent
					# Update the flag
					click_to_process = 0
			else:
				# Since the session id changed there are no clicks to process
				click_to_process = 0
				
			# Store the query timestamp
			previous_time_passed = int(parsed_line[1])
			
			
			# Get the session_id, query_id and region_id
			session_id = parsed_line[0]
			query_id = parsed_line[3]
			region_id = parsed_line[4]
			
			# Get the list of results
			results = parsed_line[5:]
			
			# Create the array with the interactions
			# The first component is the url_id
			# The second component is the number of clicks, initialized to 0
			# The third component is the time spent on that document, initialized to 0
			result_array = [[x, 0, 0] for x in results]
			
			# Add the query to the 
			queries[(session_id, query_id, region_id)] = result_array
			
		# Check the action type field
		elif parsed_line[2] == "C":
			# If the session_id is the same as the previous line
			if parsed_line[0] == session_id:
				
				# Get the identifier of the clicked document
				clicked_result = parsed_line[3]
				# Check whether the clicked document belong to the result list
				if clicked_result in results:
					# Get the index of the clicked document
					index = results.index(clicked_result)
					
					# Add the click and the time spent on the result
					queries[(session_id, query_id, region_id)][index][1] += 1
					
					# Get the current time passed
					current_time_passed = int(parsed_line[1])
					
					# Check wether there was a click in the previous iteration 
					if click_to_process:
						# Compute the time spent visiting the document
						time_spent = current_time_passed - previous_time_passed
						# Update the corresponding array entry in the hash map
						queries[(session_id, query_id, region_id)][previous_index][2] += time_spent
					
					# Store the current index and the current time passed, 
					# in case you need to update the time spent on a document on the next iteration
					previous_index = index
					previous_time_passed = current_time_passed
					
					# Change the click flag for the next interaction
					click_to_process = 1
						
		# Else it is an unknown data format so leave it out
		else:
			continue

	return queries