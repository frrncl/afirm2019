# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)


def assess_run(run, qrels):
	"""Assess a run with respect to the given qrels.

	To judge a run, for each (topic_id, doc_id, score) tuple we look up 
		that tuple in the qrels dictionary and we append the corresponding rel 
		values to the list of judged documents. 

	Args:
		run: a run imported using import_run
		qrels: a pool imported using import_qrels

	Returns:
		A judged run is represented as a dictionary where keys are topic_id and 
		values are lists of judged documents, i.e. lists of relevance degrees 
		corresponding to each document.
	"""

	# the judged run as a dictionary where keys are topic_id and values are lists of judged documents, 
	# i.e. lists of relevance degrees corresponding to each document
	jrun = {}

	for tup in run:
	    topic_id = tup[0]
	    doc_id = tup[1]
	    
	    # get the list of judged documents for the topic
	    # if it does not exist yet, create an empty list
	    l = jrun.get(topic_id, [])
	    
	    # append to the list the judgement for the current (topic_id, doc_id) pair
	    # if the pair is not in the qrels, i.e. it is unjudged, assume it is not relevant
	    l.append(qrels.get((topic_id, doc_id), 0))
	    
	    # copy back the list in the judged run
	    jrun[topic_id] = l


	return jrun
