# Copyright 2018-2019 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (maistro@dei.unipd.it)

from math import log

def discounted_cumulated_gain(jrun, logBase):
	"""Computes discounted cumulated gain.

	Args:
		jrun: an assessed run.
		logBase: the base of the logarithm used for discounting.

	Returns:
		The value of the measure.
	"""

	# the measure as a list of (topic_id, score) tuples
	measure = []

	# for each topic
	for topic_id in jrun.keys():
	    
	    # the sum of the discounted gains
	    score = 0
	    
	    # for each rank position
	    for i in range(len(jrun[topic_id])):
	               
	        # update the sum of discounted gains
	        # remember that Python uses 0-based indexing
	        # while rank positions start from 1
	        # rank positions below the log base must not be discounted
	        if(i+1 <= logBase):            
	            score += jrun[topic_id][i]
	        else:
	            score += jrun[topic_id][i] / log(i+1, logBase)
	                
	    # append the score of the measure for the given topic
	    measure.append((topic_id, score))


	return measure
